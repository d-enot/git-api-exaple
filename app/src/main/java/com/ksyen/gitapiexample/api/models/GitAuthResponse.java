package com.ksyen.gitapiexample.api.models;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class GitAuthResponse {
    @SerializedName("id")
    private String userId;
}
