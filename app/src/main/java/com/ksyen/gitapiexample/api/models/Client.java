package com.ksyen.gitapiexample.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by sergeyklymenko on 2/9/17.
 */
@Data
@AllArgsConstructor
public class Client {
    private String name;
    private String clientId;
}
