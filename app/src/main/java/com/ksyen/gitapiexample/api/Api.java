package com.ksyen.gitapiexample.api;

import com.ksyen.gitapiexample.api.models.GitAuthResponse;
import com.ksyen.gitapiexample.api.models.Repository;
import com.ksyen.gitapiexample.api.models.SearchResponce;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

public interface Api {

    @GET("/authorizations")
    Observable<List<GitAuthResponse>> getGitClient(@Header("Authorization") String header);

    @GET("/users/{user_name}/repos")
    Observable<List<Repository>> getRepos(@Path("user_name") String userName);

    @GET("/search/repositories")
    Observable<SearchResponce> getSearchedRepList(@Query("q") String query);
}
