package com.ksyen.gitapiexample.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Repository {
    private String name, description;
}
