package com.ksyen.gitapiexample.api.models;

import java.util.List;

import lombok.Data;

/**
 * Created by sergeyklymenko on 2/10/17.
 */
@Data
public class SearchResponce {

    private List<Repository> items;
}
