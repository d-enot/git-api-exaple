package com.ksyen.gitapiexample.helpers;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ksyen.gitapiexample.BuildConfig;
import com.ksyen.gitapiexample.api.Api;
import com.squareup.okhttp.OkHttpClient;

import java.lang.reflect.Modifier;

import lombok.Data;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

@Data
public class Connection {

    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss Z")
            .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
            .create();

    private static final String GIT_ENDPOINT = "https://api.github.com";

    private static Connection sInstance;

    public static Connection getInstance() {
        if (sInstance == null) {
            sInstance = new Connection();
        }
        return sInstance;
    }

    private final Api restApi;

    public Connection() {

        OkHttpClient httpClient = new OkHttpClient();
        httpClient.networkInterceptors().add(new StethoInterceptor());

        restApi = new RestAdapter.Builder()
                .setEndpoint(GIT_ENDPOINT)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(gson))
                .build()
                .create(Api.class);
    }
}
