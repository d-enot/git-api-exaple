package com.ksyen.gitapiexample;

import android.app.Activity;
import android.content.Intent;

import com.ksyen.gitapiexample.ui.SearchActivity;
import com.ksyen.gitapiexample.ui.UserRepActivity;

/**
 * Created by sergeyklymenko on 2/9/17.
 */

public class Navigator {

    public static void openRepActivity(Activity activity) {
        activity.startActivity(new Intent(activity, UserRepActivity.class));
        activity.finish();
    }

    public static void openSearchActivity(Activity activity) {
        activity.startActivity(new Intent(activity, SearchActivity.class));
    }
}
