package com.ksyen.gitapiexample;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ksyen.gitapiexample.api.models.Client;

import lombok.Getter;
import lombok.experimental.Accessors;

public class GitApplication extends Application {

    private static GitApplication mGitApplication;
    private static RequestQueue mRequestQueue;
    private static Client mClient;
    @Getter
    @Accessors(prefix = "s")
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mGitApplication = this;
        sContext = getApplicationContext();
    }

    public static GitApplication getInstance() {
        return mGitApplication;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public Client getClient() {
        return mClient;
    }

    public void setClient(Client mClient) {
        GitApplication.mClient = mClient;
    }
}
