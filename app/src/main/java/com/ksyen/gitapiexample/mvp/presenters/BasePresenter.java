package com.ksyen.gitapiexample.mvp.presenters;

import android.support.annotation.NonNull;

import com.ksyen.gitapiexample.api.Api;
import com.ksyen.gitapiexample.helpers.Connection;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

public abstract class BasePresenter<T> {

    protected Api api = Connection.getInstance().getRestApi();
    protected T view;

    public void attachView(@NonNull T view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }
}
