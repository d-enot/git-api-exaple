package com.ksyen.gitapiexample.mvp.models;

import android.util.Base64;

/**
 * Created by sergeyklymenko on 2/8/17.
 */

public class LoginInteractor {

    public static String getEncodedParams(String userName, String password) {
        String userCredentials = userName + ":" + password;
        return "Basic " + new String(Base64.encode(userCredentials.getBytes(),
                Base64.DEFAULT)).trim();
    }
}
