package com.ksyen.gitapiexample.mvp.views;

import com.ksyen.gitapiexample.api.models.Repository;

import java.util.List;

/**
 * Created by sergeyklymenko on 2/10/17.
 */

public interface SearchRepView {

    void showRepsList(List<Repository> repositories);

    void showEmptyList();

    void showProgress();
}
