package com.ksyen.gitapiexample.mvp.views;

import com.ksyen.gitapiexample.api.models.Repository;

import java.util.List;

/**
 * Created by sergeyklymenko on 2/9/17.
 */

public interface RepView {

    void showList(List<Repository> repositories);

    void showEmptyList();
}
