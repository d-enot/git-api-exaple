package com.ksyen.gitapiexample.mvp.presenters;

import com.ksyen.gitapiexample.mvp.common.RxUtils;
import com.ksyen.gitapiexample.mvp.views.SearchRepView;

import rx.Subscription;

/**
 * Created by sergeyklymenko on 2/10/17.
 */

public class SearchPresenter extends BasePresenter<SearchRepView> {

    private Subscription mSubscription;

    public void searchReps(String query) {
        view.showProgress();
        mSubscription = RxUtils.wrapAsync(api.getSearchedRepList(query))
                .subscribe(result -> {
                    if (result.getItems().size() > 0) {
                        view.showRepsList(result.getItems());
                    } else {
                        view.showEmptyList();
                    }
                }, error -> view.showEmptyList());
    }

    @Override
    public void detachView() {
        if (mSubscription != null) mSubscription.unsubscribe();
        super.detachView();
    }
}
