package com.ksyen.gitapiexample.mvp.presenters;

import com.ksyen.gitapiexample.GitApplication;
import com.ksyen.gitapiexample.api.models.Client;
import com.ksyen.gitapiexample.mvp.common.RxUtils;
import com.ksyen.gitapiexample.mvp.views.RepView;

import rx.Subscription;

/**
 * Created by sergeyklymenko on 2/9/17.
 */

public class RepPresenter extends BasePresenter<RepView> {

    private Subscription mSubscription;

    public void getUserReps() {
        Client client = GitApplication.getInstance().getClient();
        if (client != null) {
            mSubscription = RxUtils.wrapAsync(api.getRepos(client.getName()))
                    .subscribe(result -> {
                        if (result.size() > 0) {
                            view.showList(result);
                        } else {
                            view.showEmptyList();
                        }
                    }, error -> view.showEmptyList());
        } else {
            view.showEmptyList();
        }
    }

    @Override
    public void detachView() {
        if (mSubscription != null) mSubscription.unsubscribe();
        super.detachView();
    }
}
