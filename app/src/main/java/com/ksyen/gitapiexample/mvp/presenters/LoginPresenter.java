package com.ksyen.gitapiexample.mvp.presenters;

import com.ksyen.gitapiexample.GitApplication;
import com.ksyen.gitapiexample.api.models.Client;
import com.ksyen.gitapiexample.mvp.common.RxUtils;
import com.ksyen.gitapiexample.mvp.models.LoginInteractor;
import com.ksyen.gitapiexample.mvp.views.LoginView;

import rx.Subscription;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    private Subscription mSubscription;

    public void login(String userName, String password) {
        mSubscription = RxUtils.wrapAsync(api
                .getGitClient(LoginInteractor.getEncodedParams(userName, password)))
                .subscribe(result -> {
                    Client client = new Client(userName, result.get(0).getUserId());
                    GitApplication.getInstance().setClient(client);
                    view.openRepsActivity();
                }, error -> view.showError());
    }

    public boolean canLogin(String username, String password) {

        boolean canLogin = true;

        if (username.isEmpty()) {
            view.setUserNameLayoutError();
            canLogin = false;
        }

        if (password.isEmpty()) {
            view.setPasswordError();
            canLogin = false;
        }

        return canLogin;
    }

    @Override
    public void detachView() {
        if (mSubscription != null) mSubscription.unsubscribe();
        super.detachView();
    }
}
