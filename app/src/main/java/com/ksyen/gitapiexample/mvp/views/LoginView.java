package com.ksyen.gitapiexample.mvp.views;

/**
 * Created by sergeyklymenko on 2/7/17.
 */

public interface LoginView {

    void setUserNameLayoutError();

    void setPasswordError();

    void openRepsActivity();

    void showError();
}
