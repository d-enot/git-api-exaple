package com.ksyen.gitapiexample.ui;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.ksyen.gitapiexample.R;
import com.ksyen.gitapiexample.api.models.Repository;
import com.ksyen.gitapiexample.mvp.presenters.SearchPresenter;
import com.ksyen.gitapiexample.mvp.views.SearchRepView;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements SearchRepView {

    @BindView(R.id.list)
    RecyclerView mList;
    @BindView(R.id.empty_layout)
    FrameLayout mEmptyLayout;
    @BindView(R.id.progress_layout)
    FrameLayout mProgressLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindString(R.string.search_title)
    String mTitle;

    private SearchPresenter mPresenter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);
        ButterKnife.bind(this);
        initToolbar();
        initPresenter();
    }

    private void initPresenter() {
        mPresenter = new SearchPresenter();
        mPresenter.attachView(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initList(List<Repository> result) {
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setAdapter(new RepAdapter(result, this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));
        searchView.setIconified(false);
        searchView.requestFocus();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mPresenter.searchReps(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void showRepsList(List<Repository> repositories) {
        mList.setVisibility(View.VISIBLE);
        mProgressLayout.setVisibility(View.GONE);
        initList(repositories);
    }

    @Override
    public void showEmptyList() {
        mProgressLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        mEmptyLayout.setVisibility(View.GONE);
        mProgressLayout.setVisibility(View.VISIBLE);
    }
}
