package com.ksyen.gitapiexample.ui;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.ksyen.gitapiexample.Navigator;
import com.ksyen.gitapiexample.R;
import com.ksyen.gitapiexample.mvp.presenters.LoginPresenter;
import com.ksyen.gitapiexample.mvp.views.LoginView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.user_name_layout)
    TextInputLayout mUsernameLayout;
    @BindView(R.id.user_name)
    EditText mUsername;
    @BindView(R.id.password_layuot)
    TextInputLayout mPasswordLayout;
    @BindView(R.id.password)
    EditText mPassword;
    @BindString(R.string.login_error_username)
    String mErrorUsername;
    @BindString(R.string.login_error_password)
    String mErrorPassword;

    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        initPresenter();
    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    private void initPresenter() {
        mPresenter = new LoginPresenter();
        mPresenter.attachView(this);
    }

    @OnClick(R.id.login_btn) void login() {
        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();
        if (mPresenter.canLogin(username, password)) {
            mPresenter.login(username, password);
        }
    }

    @OnTextChanged(R.id.user_name) void onUserTextChanged(CharSequence s,
                                                          int start,
                                                          int before,
                                                          int count) {
        if (s.length() > 0) {
            mUsernameLayout.setErrorEnabled(false);
        }
    }

    @OnTextChanged(R.id.password) void onPasswordTextChanged(CharSequence s,
                                                             int start,
                                                             int before,
                                                             int count) {
        if (s.length() > 0) {
            mPasswordLayout.setErrorEnabled(false);
        }
    }

    @Override
    public void setUserNameLayoutError() {
        mUsernameLayout.setError(mErrorUsername);
    }

    @Override
    public void setPasswordError() {
        mPasswordLayout.setError(mErrorPassword);
    }

    @Override
    public void openRepsActivity() {
        Navigator.openRepActivity(this);
    }

    @Override
    public void showError() {
        Toast.makeText(this, getResources().getString(R.string.login_error_login),
                Toast.LENGTH_LONG).show();
    }
}
