package com.ksyen.gitapiexample.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ksyen.gitapiexample.R;
import com.ksyen.gitapiexample.api.models.Repository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepAdapter extends RecyclerView.Adapter<RepAdapter.ViewHolder>{

    private List<Repository> mItems;
    private Context mContext;

    public RepAdapter(List<Repository> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rep_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = mItems.get(position);
        holder.name.setText(repository.getName());
        holder.description.setText(repository.getDescription());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.description)
        TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
