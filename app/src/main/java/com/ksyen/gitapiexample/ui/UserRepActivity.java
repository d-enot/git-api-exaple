package com.ksyen.gitapiexample.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.ksyen.gitapiexample.Navigator;
import com.ksyen.gitapiexample.R;
import com.ksyen.gitapiexample.api.models.Repository;
import com.ksyen.gitapiexample.mvp.presenters.RepPresenter;
import com.ksyen.gitapiexample.mvp.views.RepView;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserRepActivity extends AppCompatActivity implements RepView {

    @BindView(R.id.list)
    RecyclerView mList;
    @BindView(R.id.empty_layout)
    FrameLayout mEmptyLayout;
    @BindView(R.id.progress_layout)
    FrameLayout  mProgressLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindString(R.string.user_rep_title)
    String mTitle;

    private RepPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_rep_activity);
        ButterKnife.bind(this);
        initPresenter();
        initToolBar();
        mPresenter.getUserReps();
    }

    private void initPresenter() {
        mPresenter = new RepPresenter();
        mPresenter.attachView(this);
    }

    private void initToolBar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_rep_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Navigator.openSearchActivity(this);
        return true;
    }

    @Override
    public void showList(List<Repository> repositories) {
        mList.setVisibility(View.VISIBLE);
        mProgressLayout.setVisibility(View.GONE);
        mList.setLayoutManager(new LinearLayoutManager(this));
        mList.setAdapter(new RepAdapter(repositories, this));
    }

    @Override
    public void showEmptyList() {
        mProgressLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);
    }
}
